# ESERCIZIO 3

- Create un repository nella seguente situazione:
  + 2 commit
  + index diverso da HEAD
  + working directory diversa da index
  + almeno un file della working directory non versionato 

- Copiare tale repository in due directory distinte e risolvere il seguente
  problema mediante due approcci diversi, in un caso tramite l'uso del comando
  stash, che è invece vietato nel secondo caso:

  + avendo ricevuto l'incarico di aggiungere urgentemente un file BUGFIX (va
    bene anche vuoto per noi) al repository, apportare la modifica del
    repository richiesto in maniera tale da potere subito dopo ripartire dalla
    situazione creata inizialmente

- Consegnare un file SoluzioneEs3.zip contenente:

  + le due directory con i due repo modificati
  + un file contenente la spiegazione dei comandi usati
  + le due registrazioni delle sessioni di shell con cui avete risolto
    l'esercizio
  
  
Per registrare la sessione di shell usare il comando:

asciinema rec es3sol1

...svolgere esercizio...

dare il comando exit

non è un problema se nella registrazione ci sono pause lunghe o altre cose prima
o dopo la soluzione dell'esercizio
